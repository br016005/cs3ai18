# CS3AI18


For the Document: Please Visit https://docs.google.com/document/d/16sbVGAWWgS3oq9c8mTdt8cLrN0finxeYJXwYjzyfEPY/edit?usp=sharing
For the CSGitLab repo: Please Visit https://csgitlab.reading.ac.uk/br016005/cs3ai18
## Instructions:

Both files were developed in PyCharm, using Python 3.9.1 64-bit.

To run _ga_continuous-distrib.py_ or _ga-combinatorial-distrib.py_, from this folder:

```bash
<PATH/TO/PYTHON/INTERPRETER>.exe ./ga_continuous-distrib.py
<PATH/TO/PYTHON/INTERPRETER>.exe ./ga-combinatorial-distrib.py
```
To access the different problems that each GA implementation is setup to solve, the `FITNESS_CHOICE` variable can be changed within the file. 
Alternatively, upon execution, the user should be prompted which problem to approach. 

### FITNESS_CHOICE Value:

| Value | _ga_continuous-distrib.py_ | _ga-combinatorial-distrib.py_ |
|-------|----------------------------|-------------------------------|
| 1     | Sum Squares                | Sum 1s (Minimisation)         |
| 2     | Levy		             | Knapsack Problem		     |
| 3     | Griewank 		     | N Queens Problem		     |
| 4     | Perm                       | map Coloring 		     |
| 5     | Trid	                     | Travelling Salesman	     |
| 6     | Zakharov                   | 				     |  



 See the code, or the report for further details on which problems are being solved. 
this was possible to be done via parsing command line arguments, but this was unnecessary as it can be handled via inputs. 
Also, this coursework was already complex enough without making in harder.. 

